package com.shivg7706.login;

import com.shivg7706.login.model.ERole;
import com.shivg7706.login.model.Role;
import com.shivg7706.login.repository.RoleRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class LoginApplicationTests {

	@Autowired
	RoleRepository roleRepository;

	@Test
	void contextLoads() {
	}

	@Test
	void insertRoles() {
		Role r1 = new Role();
		r1.setName(ERole.ADMIN);
		Role r2 = new Role();
		r2.setName(ERole.MOD);
		Role r3 = new Role();
		r3.setName(ERole.USER);

		roleRepository.save(r1);
		roleRepository.save(r2);
		roleRepository.save(r3);
	}

}
